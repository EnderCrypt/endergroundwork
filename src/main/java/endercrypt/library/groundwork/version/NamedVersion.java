package endercrypt.library.groundwork.version;

public class NamedVersion extends Version
{
	private static final long serialVersionUID = 2191433037029935388L;
	
	/**
	 * 
	 */
	
	public static NamedVersion fromArray(String[] names, int[] version)
	{
		Version basicVersion = Version.fromArray(version);
		
		if (basicVersion.getMajor() > names.length - 1)
		{
			throw new IllegalArgumentException("Major version " + basicVersion.getMajor() + " does not have a name");
		}
		String name = names[basicVersion.getMajor()];
		
		return new NamedVersion(name, basicVersion);
	}
	
	private final String name;
	
	public NamedVersion(String name, Version version)
	{
		super(version);
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	@Override
	public String toString()
	{
		return super.toString() + " \"" + getName() + "\"";
	}
}
