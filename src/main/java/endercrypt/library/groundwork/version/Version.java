package endercrypt.library.groundwork.version;


import java.io.Serializable;


public class Version implements Serializable
{
	private static final long serialVersionUID = 7570800193611660738L;
	
	/**
	 * 
	 */
	
	public static Version fromArray(int[] version)
	{
		int i = 0;
		return new Version(version[i++], version[i++], version[i++]);
	}
	
	private static final String SEPARATOR = ".";
	
	private final int major;
	private final int minor;
	private final int patch;
	
	protected Version(Version version)
	{
		this(version.getMajor(), version.getMinor(), version.getPatch());
	}
	
	public Version(int major, int minor, int patch)
	{
		this.major = major;
		this.minor = minor;
		this.patch = patch;
	}
	
	public int getMajor()
	{
		return major;
	}
	
	public int getMinor()
	{
		return minor;
	}
	
	public int getPatch()
	{
		return patch;
	}
	
	public String toVersionNumbers()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getMajor()).append(SEPARATOR);
		sb.append(getMinor()).append(SEPARATOR);
		sb.append(getPatch());
		return sb.toString();
	}
	
	@Override
	public String toString()
	{
		return toVersionNumbers();
	}
}
