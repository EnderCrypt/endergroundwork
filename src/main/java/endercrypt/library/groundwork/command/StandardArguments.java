package endercrypt.library.groundwork.command;


import picocli.CommandLine.Command;


@Command(mixinStandardHelpOptions = true, helpCommand = true)
public class StandardArguments
{
	public StandardArguments()
	{
		// protected
	}
	
	@SuppressWarnings("unused")
	protected <A extends StandardArguments, C extends EnderCommandLineParser<A>> void verify(C commandLine)
	{
		// do nothing
	}
}
