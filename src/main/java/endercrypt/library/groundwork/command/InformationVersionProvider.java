package endercrypt.library.groundwork.command;


import endercrypt.library.groundwork.information.Information;

import java.util.Objects;

import com.github.tomaslanger.chalk.Chalk;

import picocli.CommandLine.IVersionProvider;


class InformationVersionProvider implements IVersionProvider
{
	private final Information information;
	
	public InformationVersionProvider(Information information)
	{
		this.information = Objects.requireNonNull(information, "information");
	}
	
	@Override
	public String[] getVersion() throws Exception
	{
		return new String[] {
			Chalk.on(information.getName() + " " + information.getVersion()).bgWhite().black().toString(),
			"Created by " + information.getAuthor(),
			"Repository: " + Chalk.on(information.getRepository()).underline() };
	}
}
