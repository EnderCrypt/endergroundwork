package endercrypt.library.groundwork.command;


import endercrypt.library.commons.JarInfo;
import endercrypt.library.groundwork.information.Information;

import java.io.PrintWriter;

import picocli.CommandLine;
import picocli.CommandLine.Model.CommandSpec;


public class EnderCommandLineParser<T extends StandardArguments> extends CommandLine
{
	private final Information information;
	private final T arguments;
	
	public EnderCommandLineParser(Information information, T arguments)
	{
		super(arguments);
		this.information = information;
		this.arguments = arguments;
		
		setCommandName(getInformation().getName());
		getCommandSpec().usageMessage().description(getInformation().getDescription());
		setUsageHelpAutoWidth(true);
		setCaseInsensitiveEnumValuesAllowed(true);
		
		CommandSpec commandSpec = getCommandSpec();
		commandSpec.name(JarInfo.of(getClass()).getFilename());
		commandSpec.versionProvider(new InformationVersionProvider(information));
	}
	
	public Information getInformation()
	{
		return information;
	}
	
	public T getArguments()
	{
		return arguments;
	}
	
	public ParseResult parsePicocli(String[] args)
	{
		ParseResult result = silentlyParsePicocli(args);
		
		if (result == null)
		{
			return null;
		}
		
		if (result.isUsageHelpRequested())
		{
			usage(getOut());
			System.exit(0);
			return null;
		}
		if (result.isVersionHelpRequested())
		{
			printVersionHelp(getOut());
			System.exit(0);
			return null;
		}
		
		return result;
	}
	
	private ParseResult silentlyParsePicocli(String[] args)
	{
		try
		{
			ParseResult parseResult = parseArgs(args);
			arguments.verify(this);
			return parseResult;
		}
		catch (ParameterException e)
		{
			// writer
			PrintWriter writer = getErr();
			
			// error
			writer.write(getColorScheme().errorText(e.getMessage()).toString() + "\n");
			writer.flush();
			
			// help
			usage(writer);
			writer.flush();
			
			// finish
			return null;
		}
	}
}
