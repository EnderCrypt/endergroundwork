package endercrypt.library.groundwork.information;


import endercrypt.library.groundwork.version.Version;


public class Information
{
	public static Information assemble(RawInformation rawInformation)
	{
		Version version = Version.fromArray(rawInformation.getVersion());
		return assemble(rawInformation, version);
	}
	
	public static Information assemble(RawInformation rawInformation, Version version)
	{
		String name = rawInformation.getName();
		String author = rawInformation.getAuthor();
		String repository = rawInformation.getRepository();
		String rootPackage = rawInformation.getPackage();
		
		return new Information(name, author, repository, rootPackage, version, rootPackage);
	}
	
	public Information(String name, String author, String repository, String rootPackage, Version version, String description)
	{
		this.name = name;
		this.author = author;
		this.repository = repository;
		this.rootPackage = rootPackage;
		this.version = version;
		this.description = description;
	}
	
	private String name;
	
	public String getName()
	{
		return name;
	}
	
	private String author;
	
	public String getAuthor()
	{
		return author;
	}
	
	private String repository;
	
	public String getRepository()
	{
		return repository;
	}
	
	private String rootPackage;
	
	public String getPackage()
	{
		return rootPackage;
	}
	
	private Version version;
	
	public Version getVersion()
	{
		return version;
	}
	
	private String description;
	
	public String getDescription()
	{
		return description;
	}
}
