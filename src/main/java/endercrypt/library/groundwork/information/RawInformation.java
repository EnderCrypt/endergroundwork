package endercrypt.library.groundwork.information;


import java.nio.file.Path;
import java.nio.file.Paths;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.introspector.BeanAccess;


public class RawInformation
{
	private static final Yaml yamlParser = new Yaml();
	
	static
	{
		yamlParser.setBeanAccess(BeanAccess.FIELD);
	}
	
	public static RawInformation parse(String yaml)
	{
		return yamlParser.loadAs(yaml, RawInformation.class);
	}
	
	public static final Path defaultFilePath = Paths.get("./information.yaml");
	
	private String name;
	
	public String getName()
	{
		return name;
	}
	
	private String author;
	
	public String getAuthor()
	{
		return author;
	}
	
	private String repository;
	
	public String getRepository()
	{
		return repository;
	}
	
	private String rootPackage;
	
	public String getPackage()
	{
		return rootPackage;
	}
	
	private int[] version;
	
	public int[] getVersion()
	{
		return version;
	}
	
	private String description;
	
	public String getDescription()
	{
		return description;
	}
}
