package endercrypt.library.groundwork.launcher;


import endercrypt.library.commons.data.DataSource;
import endercrypt.library.groundwork.command.EnderCommandLineParser;
import endercrypt.library.groundwork.command.StandardArguments;
import endercrypt.library.groundwork.information.Information;
import endercrypt.library.groundwork.information.RawInformation;

import java.io.IOException;
import java.lang.reflect.Constructor;

import net.jodah.typetools.TypeResolver;


public abstract class LinearApplication<A extends StandardArguments>
{
	public static <A extends StandardArguments, T extends LinearApplication<A>> T launch(Class<T> mainClass, String[] args)
	{
		// create main
		T application = createApplication(mainClass);
		
		// information
		Information information = createInformation();
		
		// arguments
		A arguments = createArguments(mainClass, information, args);
		
		// launch
		try
		{
			application.main(arguments, information);
		}
		catch (Exception e)
		{
			throw new LinearApplicationLaunchException(application.getClass() + ".main crashed", e);
		}
		return application;
	}
	
	private static <T> T constructClassInstance(Class<T> targetClass, String name)
	{
		try
		{
			Constructor<T> argumentClassConstructor = targetClass.getConstructor();
			return argumentClassConstructor.newInstance();
		}
		catch (ReflectiveOperationException e)
		{
			throw new LinearApplicationLaunchException("Failed to construct " + name + " " + targetClass, e);
		}
	}
	
	private static <A extends StandardArguments, T extends LinearApplication<A>> T createApplication(Class<T> mainClass)
	{
		return constructClassInstance(mainClass, "main");
	}
	
	@SuppressWarnings("unchecked")
	private static <A extends StandardArguments, T extends LinearApplication<A>> A createArguments(Class<T> mainClass, Information information, String[] args)
	{
		Class<A> argumentClass = (Class<A>) TypeResolver.resolveRawArgument(LinearApplication.class, mainClass);
		A arguments = constructClassInstance(argumentClass, "argument");
		
		EnderCommandLineParser<A> commandLine = new EnderCommandLineParser<>(information, arguments);
		
		if (commandLine.parsePicocli(args) == null)
		{
			System.exit(1);
		}
		
		return arguments;
	}
	
	private static Information createInformation()
	{
		try
		{
			String yaml = DataSource.RESOURCES.read(RawInformation.defaultFilePath).asString();
			RawInformation rawInformation = RawInformation.parse(yaml);
			Information information = Information.assemble(rawInformation);
			return information;
		}
		catch (IOException e)
		{
			throw new LinearApplicationLaunchException("Failed to read data from " + RawInformation.defaultFilePath, e);
		}
	}
	
	public abstract void main(A arguments, Information information) throws Exception;
}
