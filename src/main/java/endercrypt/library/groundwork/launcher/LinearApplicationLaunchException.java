package endercrypt.library.groundwork.launcher;

public class LinearApplicationLaunchException extends RuntimeException
{
	private static final long serialVersionUID = -2580826793150880766L;
	
	public LinearApplicationLaunchException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
