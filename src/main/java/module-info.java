
/**
 * @author endercrypt
 */
module EnderGroundwork
{
	requires info.picocli;
	requires org.yaml.snakeyaml;
	requires net.jodah.typetools;
	requires chalk;
	requires EnderCommons;
	requires lombok;
	
	exports endercrypt.library.groundwork.command;
	exports endercrypt.library.groundwork.information;
	exports endercrypt.library.groundwork.launcher;
	exports endercrypt.library.groundwork.version;
	
	opens endercrypt.library.groundwork.information;
}
